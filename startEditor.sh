#!/bin/bash
sqlite=$(find $HOME/.gradle/caches/modules-2/files-2.1/org.xerial/sqlite-jdbc/*/*/*.jar -printf %p:)
database=$(find ./Database/build/libs/Database-*.jar -printf %p:)
editor=$(find ./Editor/build/libs/Editor-*.jar -printf %p:)
java  --module-path ./javafx-sdk-*/lib/ --add-modules javafx.fxml,javafx.controls -cp "$sqlite":"$database":"$editor" cz.sspbrno.team18.editor.Editor
