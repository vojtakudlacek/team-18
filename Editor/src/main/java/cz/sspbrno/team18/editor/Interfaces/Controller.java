package cz.sspbrno.team18.editor.Interfaces;

import cz.sspbrno.team18.database.Path.Polygon;
import cz.sspbrno.team18.database.connector.SQLiteDBConnector;

import java.util.ArrayList;

public interface Controller {
    int getLastIndex();

    ArrayList<Polygon> getPolygonArrayList();

    Polygon getPolygon();

    SQLiteDBConnector getDbConnector();

    void reDraw();

    void setPolygon(Polygon polygon);

    void setLastIndex(int lastIndex);
}
