package cz.sspbrno.team18.editor.JMLSP;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Set;

public class JMLSP {
    private String languagesFolder;
    private HashMap<String, HashMap<String, String>> languages;
    private String selectedLanguage;

    public JMLSP(String languagesFolder, String selectedLanguage) throws IOException {
        this.languagesFolder = languagesFolder;
        languages = new HashMap<>();
        this.selectedLanguage = selectedLanguage;
        loadFromFiles();
    }

    public Set<String> getLanguages() {
        return languages.keySet();
    }

    private void loadFromFiles() throws IOException {
        File file = new File(this.languagesFolder);
        file.deleteOnExit();
        if (!file.isDirectory()) {
            file.mkdirs();
            ClassLoader classLoader = getClass().getClassLoader();
            Files.copy(new File(classLoader.getResource("en_us.lang").getPath()).toPath(), new File(languagesFolder + "/en_us.lang").toPath(), StandardCopyOption.REPLACE_EXISTING);
        }

        File[] files = file.listFiles();
        assert files != null;
        for (File f : files) {
            BufferedReader br = new BufferedReader(new FileReader(f));
            String line;
            HashMap<String, String> tmp = new HashMap<>();
            while ((line = br.readLine()) != null) {
                tmp.put(line.split(";")[0], line.split(";")[1]);
            }
            languages.put(f.getName(), tmp);
        }

    }

    public void setLanguage(String language) {
        this.selectedLanguage = language;
    }

    public String getByID(String ID) {
        return languages.get(selectedLanguage).get(ID);
    }
}
