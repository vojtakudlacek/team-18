# Team 18

## Build instructions
### Requirements:
   * Java 1.12
   * JavaFX SDK
   * Gradle build tool 
### Build:
* Use ```$ gradle build -p Editor``` to build editor
* Use ```$ gradle build -p Control``` to build cz.sspbrno.team18.control app
* Or if you are on linux you can just use ```./build.sh```

## JavaFX
* For run the program you need JavaFX SDK
### Linux
* Use ```./downloadFx.sh```
### Windows
* Download and Unzip JavaFX in the project folder
## Run:
### Linux
* Editor: ```./startEditor.sh```
* Control ```./startControl.sh```
### Windows
* There will be .bat files in the future ...

## JavaFX in InteliJ:
For some reason gradle couldn't find modules

You have to add this path to load JavaFX modules if they are locally on your PC
```--module-path /path/to/javafx-sdk-version/lib --add-modules=javafx.fxml,javafx.controls```