package cz.sspbrno.team18.database;


public enum  Constants {
    OBJECTS_TABLE("OBJECTS", "("+
            " 'ID'     INTEGER  PRIMARY KEY AUTOINCREMENT," +
            " 'NAME'           VARCHAR(45)" +
            ")", "(NAME)"),
    POINTS_TABLE("POINTS", "(" +
            " 'ID' INTEGER PRIMARY KEY AUTOINCREMENT," +
            " 'X'           FLOAT    NOT NULL, " +
            " 'Y'           FLOAT    NOT NULL, " +
            " 'OBJECTID' INTEGER NOT NULL, "+
            " CONSTRAINT `fk_Point_Object1` FOREIGN KEY ('OBJECTID') REFERENCES "+OBJECTS_TABLE.name+"('ID')"+
            ")", "(X, Y, OBJECTID)");

    public String name;
    public String createValues;
    public String normalValues;

    Constants(String name, String createValues, String normalValues){
        this.name = name;
        this.createValues = createValues;
        this.normalValues = normalValues;
    }
}
