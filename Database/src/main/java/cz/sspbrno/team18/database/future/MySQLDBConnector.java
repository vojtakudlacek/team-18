package cz.sspbrno.team18.database.future;

import java.sql.*;
import java.util.HashMap;

public class MySQLDBConnector
{
    private Connection conn;

    /**
     * Constructs DBConnection for MySql
     * @param host - IP of the MySql server
     * @param port - Port of the MySql server
     * @param username - Used to connect to Mysql server
     * @param password - Used to connect to MySql server
     * @param DBname - Name of the database you will use
     * @throws SQLException
     */
    public MySQLDBConnector(String host, int port, String username, String password, String DBname) throws SQLException, ClassNotFoundException
    {
        Class.forName("com.mysql.jdbc.Driver");
        String URL = "jdbc:mysql://"+host+":"+port+"/"+DBname;
        this.conn = DriverManager.getConnection(URL,username,password);
    }

    /**
     * Constructs MySQLDBConnector for SQLite
     * @param fileName - name of the DB file
     * @throws SQLException
     */
    public MySQLDBConnector(String fileName) throws SQLException, ClassNotFoundException
    {
        Class.forName("org.sqlite.JDBC");
        this.conn = DriverManager.getConnection("jdbc:sqlite:"+fileName);
    }

    public ResultSet querrySQL(String SQL) throws SQLException
    {
        PreparedStatement prpstm = conn.prepareStatement(SQL);
        ResultSet rs = prpstm.executeQuery();
        return rs;
    }

}
