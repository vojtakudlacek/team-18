#!/bin/bash
sqlite=$(find $HOME/.gradle/caches/modules-2/files-2.1/org.xerial/sqlite-jdbc/*/*/*.jar -printf %p:)
database=$(find ./Database/build/libs/Database-*.jar -printf %p:)
guava=$(find $HOME/.gradle/caches/modules-2/files-2.1/com.google.guava/guava/*/*/*.jar -printf %p:)
control=$(find ./Control/build/libs/Control-*.jar -printf %p:)
editor=$(find ./Editor/build/libs/Editor-*.jar -printf %p:)
java  --module-path ./javafx-sdk-*/lib/ --add-modules javafx.fxml,javafx.controls -cp "$sqlite":"$database":"$editor":"$control" cz.sspbrno.team18.control.Control
