package cz.sspbrno.team18.control.misc;

import cz.sspbrno.team18.control.scenes.FlightController;
import cz.sspbrno.team18.database.Path.Point;
import cz.sspbrno.team18.control.connection.CommandSender;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.Collections;

public class Flying implements Runnable {
    private boolean isUp = false;
    private volatile CommandSender sender;
    private int height;
    private int speed;
    private Text position;
    private volatile ArrayList<Point> points;

    public Flying(FlightController controller, CommandSender sender) {
        this.sender = sender;
        this.position = controller.position;
    }

    @Override
    public void run() {
        if (points != null) {
            CommandSender sender = this.sender;
            ArrayList<Point> points = new ArrayList<>(this.points);
            this.points.clear();

            if (!isUp) {
                System.out.println("Vzlétávám");
                sender.command("takeoff", false);
                isUp = true;
                System.out.println("Nastavuji výšku");
                position.setText("Souřadnice dalšího bodu letu: x: 0 y: " + height + " z: 0");
                sender.command("go 0 0 " + height + " " + speed, false);
                while (points.size() > 0 && !Thread.currentThread().isInterrupted()) {
                    Point point = points.remove(0);
                    if (point.getX() != 0 || point.getY() != 0) {
                        if (point.getX() < 20 && point.getX() > 0) {
                            point = new Point(+21, point.getY());
                        } else if (point.getX() < 0 && point.getX() > -20) {
                            point = new Point(-21, point.getY());
                        }

                        if (point.getY() < 20 && point.getY() > 0) {
                            point = new Point(point.getX(), +21);
                        } else if (point.getY() < 0 && point.getY() > -20) {
                            point = new Point(point.getX(), -21);
                        }

                        position.setText("Souřadnice dalšího bodu letu: x: " + point.getX() + " y: " + height + " z: " + point.getY());
                        sender.command("go " + (int) point.getX() + " " + (int) point.getY() + " 0 " + speed, true);
                    }
                }
                System.out.println("Není nastaven další bod, přístávám");
                System.out.println("Musíš si vybrat polygon");
            } else {
                System.out.println("Počkej na přístání");
            }
            sender.command("land", true);
            isUp = false;
        }
    }

    public void update(ArrayList<Point> points, double height, double speed) {
        this.points = points;
        this.height = (int) height;
        this.speed = (int) speed;
    }

    public void setUp() {
        this.isUp = false;
    }

    public boolean isUp() {
        return isUp;
    }
}
