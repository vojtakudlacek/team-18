package cz.sspbrno.team18.control.scenes;

import cz.sspbrno.team18.control.Control;

public class WelcomeScene {
    public void setApplication(Control controlApp) {
        controlApp.changeScene("flightControl.fxml");
    }
}
