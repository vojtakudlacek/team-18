package cz.sspbrno.team18.control;

import cz.sspbrno.team18.control.scenes.FlightController;
import cz.sspbrno.team18.control.scenes.WelcomeScene;
import cz.sspbrno.team18.editor.JMLSP.JMLSP;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;

public class Control extends Application {
    private Stage primaryStage;
    public static JMLSP jmlsp;

    @Override
    public void start(Stage primaryStage) throws Exception {
        jmlsp = new JMLSP(System.getProperty("user.home") + "/.team-18/jmlsp", "en_us.lang");
        Object[] options = jmlsp.getLanguages().toArray();
        jmlsp.setLanguage((String) options[JOptionPane.showOptionDialog(null, "Select language"
                , "Language", 1, 1, null, options, "en_us.lang")]);
        this.primaryStage = primaryStage;
        FXMLLoader resources = new FXMLLoader(this.getClass().getClassLoader().getResource("welcome.fxml"));
        Parent root = resources.load();
        WelcomeScene mainScene = resources.getController();
        primaryStage.setTitle("Drone Control");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        primaryStage.setFullScreen(true);

        mainScene.setApplication(this);
        System.out.println("Application started");
    }

    public void changeScene(String fxml) {
        try {
            FXMLLoader resource = new FXMLLoader(this.getClass().getClassLoader().getResource(fxml));
            Parent root = resource.load();
            FlightController flightController = resource.getController();
            flightController.setApplication(this);
            primaryStage.setScene(new Scene(root));
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void closeLastStage() throws Exception {
        primaryStage.close();
        start(new Stage());
    }
}
