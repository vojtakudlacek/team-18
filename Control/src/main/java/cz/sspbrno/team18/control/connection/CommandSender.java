package cz.sspbrno.team18.control.connection;

import cz.sspbrno.team18.control.Control;
import cz.sspbrno.team18.control.misc.Constants;
import cz.sspbrno.team18.control.scenes.FlightController;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;

public class CommandSender {
    private DatagramSocket socket;
    private InetAddress address;
    private Text battery;
    private Text time;

    public CommandSender(FlightController flightController) {
        battery = flightController.battery;
        time = flightController.time;
        try {
            this.address = InetAddress.getByName(Constants.ADDRESS);
            socket = new DatagramSocket();
        } catch (SocketException | UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void command(String msg, boolean check) {
        if (check) {
            battery.setText(Control.jmlsp.getByID("Control.Battery") + status("battery?"));
            time.setText(Control.jmlsp.getByID("Control.Time") + status("time?"));
        }

        try {
            byte[] buf = msg.getBytes();
            DatagramPacket packet = new DatagramPacket(buf, buf.length, address, Constants.PORT);
            socket.send(packet);
            buf = new byte[100];
            packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);

            String doneText = new String(packet.getData(), 0, packet.getLength(), StandardCharsets.UTF_8);

            System.out.println(doneText);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String status(String msg) {
        try {
            byte[] buf = msg.getBytes();
            DatagramPacket packet = new DatagramPacket(buf, buf.length, address, Constants.PORT);
            socket.send(packet);
            buf = new byte[100];
            packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);
            return new String(packet.getData(), 0, packet.getLength(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

}
