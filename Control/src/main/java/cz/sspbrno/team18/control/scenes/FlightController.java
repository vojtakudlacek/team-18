package cz.sspbrno.team18.control.scenes;

import cz.sspbrno.team18.control.connection.CommandSender;
import cz.sspbrno.team18.control.misc.Constants;
import cz.sspbrno.team18.control.misc.Flying;
import cz.sspbrno.team18.control.Control;
import cz.sspbrno.team18.database.Path.Point;
import cz.sspbrno.team18.database.Path.Polygon;
import cz.sspbrno.team18.database.connector.SQLiteDBConnector;
import cz.sspbrno.team18.editor.Interfaces.Controller;
import cz.sspbrno.team18.editor.Utils;
import cz.sspbrno.team18.editor.myEventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Spinner;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;

public class FlightController implements Controller {
    private SQLiteDBConnector dbConnector;
    private volatile ArrayList<Polygon> polygonArrayList;
    private int lastIndex = -1;
    private boolean control = false;
    private Polygon polygon;
    private Control controlApp;
    private Thread flyingThread;
    private Thread videoStream;
    private Flying flying;
    private CommandSender sender;
    private ArrayList<Point> points;
    @FXML
    private Canvas canvas;

    @FXML
    private MenuButton paths;

    @FXML
    public Text position;

    @FXML
    public Text battery;

    @FXML
    public Text time;

    @FXML
    private Spinner<Double> height;

    @FXML
    private Spinner<Double> speed;
    @FXML
    private Button Connect;
    @FXML
    private Button Fly;
    @FXML
    private Button Stop;
    @FXML
    private Button Restart;
    @FXML
    private Button StartVideo;
    @FXML
    private Button StopVideo;
    @FXML
    private Text Height;
    @FXML
    private Text Speed;

    @FXML
    public void initialize() {
        setTexts();
        polygonArrayList = new ArrayList<>();
        try {
            dbConnector = new SQLiteDBConnector();
            polygonArrayList.addAll(dbConnector.load());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, Control.jmlsp.getByID("Global.DatabaseLoadError") + e.getMessage(), Control.jmlsp.getByID("Global.Error"), JOptionPane.ERROR_MESSAGE);
        }

        for (Polygon pol : polygonArrayList) {
            MenuItem path = new MenuItem(pol.getName());
            path.setOnAction(new myEventHandler(this));
            paths.getItems().add(path);
        }

        drawCanvas();
        System.out.println("Window initialize successfully");
    }

    private void setTexts() {
        this.Connect.setText(Control.jmlsp.getByID("Control.Connect"));
        this.Fly.setText(Control.jmlsp.getByID("Control.Fly"));
        this.Stop.setText(Control.jmlsp.getByID("Control.Stop"));
        this.Restart.setText(Control.jmlsp.getByID("Control.Restart"));
        this.StartVideo.setText(Control.jmlsp.getByID("Control.StartVideo"));
        this.StopVideo.setText(Control.jmlsp.getByID("Control.StopVideo"));
        this.Height.setText(Control.jmlsp.getByID("Control.Height"));
        this.Speed.setText(Control.jmlsp.getByID("Control.Speed"));
        this.battery.setText(Control.jmlsp.getByID("Control.Battery"));
        this.time.setText(Control.jmlsp.getByID("Control.Time"));
        this.paths.setText(Control.jmlsp.getByID("Control.LoadPath"));
    }

    @FXML
    public void startVideo() {
        sender.command("streamon", false);
        videoStream = new Thread(() -> {
            try {
                Runtime.getRuntime().exec("ffmpeg -i udp://0.0.0.0:11111 -f sdl Tello");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        videoStream.start();
    }

    @FXML
    public void stopVideo() {
        sender.command("streamoff", false);
        try {
            videoStream.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void start() {
        sender = new CommandSender(this);
        if (!control) {
            sender.command("command", false);
            control = true;
        }
        flying = new Flying(this, sender);
    }

    @FXML
    public void stop() {
        if (flying.isUp()) {
            flyingThread.interrupt();
            sender.command("stop", false);
            flying.setUp();
        }
    }

    @Override
    public int getLastIndex() {
        return lastIndex;
    }

    @Override
    public ArrayList<Polygon> getPolygonArrayList() {
        return polygonArrayList;
    }

    @Override
    public Polygon getPolygon() {
        return polygon;
    }

    @Override
    public SQLiteDBConnector getDbConnector() {
        return dbConnector;
    }

    @FXML
    public void fly() {
        if (flyingThread == null || !flyingThread.isAlive()) {
            flying.update(points, height.getValue(), speed.getValue());
            flyingThread = new Thread(flying);
            flyingThread.start();
        } else {
            JOptionPane.showMessageDialog(null, Control.jmlsp.getByID("Control.WaitToEndOfFly"), Control.jmlsp.getByID("Global.Error"), JOptionPane.WARNING_MESSAGE);
            System.out.println(Control.jmlsp.getByID("Control.WaitToEndOfFly"));
        }
    }

    @Override
    public void reDraw() {
        points = new ArrayList<>();
        points.add(polygon.get(0));
        for (int i = 1; i < polygon.size(); i++) {
            float posX = polygon.get(i).getX();
            float previousPosX = polygon.get(i - 1).getX();
            posX = posX - previousPosX;

            float posY = polygon.get(i).getY();
            float previousPosY = polygon.get(i - 1).getY();
            posY = posY - previousPosY;

            points.add(new Point(posX, posY));
        }

        GraphicsContext graphicsContext = drawCanvas();

        if (lastIndex != polygonArrayList.indexOf(polygon) && lastIndex != -1) {
            lastIndex = polygonArrayList.indexOf(polygon);
        }

        graphicsContext.setStroke(Color.BLACK);

        if (polygon != null) {
            for (int i = 1; i < polygon.size(); i++) {
                graphicsContext.beginPath();

                Point lastPoint = Utils.calculateToDraw(polygon, canvas.getWidth(), canvas.getHeight()).get(i - 1);
                Point nowPoint = Utils.calculateToDraw(polygon, canvas.getWidth(), canvas.getHeight()).get(i);

                graphicsContext.moveTo(lastPoint.getX(), lastPoint.getY());
                graphicsContext.lineTo(nowPoint.getX(), nowPoint.getY());

                graphicsContext.stroke();
            }
        }
    }

    @Override
    public void setLastIndex(int lastIndex) {
        this.lastIndex = lastIndex;
    }

    @Override
    public void setPolygon(Polygon polygon) {
        this.polygon = polygon;
    }

    public void setApplication(Control controlApp) {
        this.controlApp = controlApp;
    }

    @FXML
    public void restart() {
        try {
            controlApp.closeLastStage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private GraphicsContext drawCanvas() {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.setLineWidth(1.0);

        graphicsContext.setStroke(Color.WHITE);

        graphicsContext.setFill(Color.gray(0.90));
        graphicsContext.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        return graphicsContext;
    }
}